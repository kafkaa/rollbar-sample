// Go implementation of the Bash tree command using Rollbar API for error checking
package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/rollbar/rollbar-go"
)

const indent int = 3

func rbInit() {
	rollbar.SetToken(os.Getenv("RBTOKEN"))
	rollbar.SetEnvironment("production")
	rollbar.SetCodeVersion("v2")
	rollbar.SetServerRoot("/")
}

func validateArgs() (string, error) {
	if len(os.Args) != 2 {
		return "", errors.New("missing root directory")
	}

	return os.Args[1], nil
}

func printTree(r string, i int) {
	if files, err := ioutil.ReadDir(r); err != nil {
		log.Fatal(err)
	} else {
		for _, f := range files {
			fmt.Printf("%*s%s\n", i, " ", f.Name())
			if f.IsDir() {
				i += indent
				printTree(fmt.Sprintf("%s/%s", r, f.Name()), i)
				i -= indent
			}
		}
	}
}

func main() {

	rbInit()

	root, err := validateArgs()

	if err != nil {
		rollbar.Critical(err)
		rollbar.Info("arguments not validated")
		rollbar.Wait()
	}

	fmt.Println(root)
	printTree(root, indent)
}
